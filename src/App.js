import React from "react";
import axios from "axios";
import "./App.css";

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

function App() {
  const classes = useStyles();

  return (
    <div className="App">
      <div class="sidenav">
        <a><img src={require("./Logo New Type.jpg")} alt="theLogo" /></a>
        <p>Why don't we visit...</p>
        <a href="#">About</a>
        <a href="#">Services</a>
        <a href="#">Clients</a>
        <a href="#">Contact</a>
      </div>
      <div id="grid">

        <div id="header">
          <header id="mainTitle">Home</header>
          <header id="subTitle">By Zachary Ferretti</header>
        </div>
        <div id="siteBody" flex>
          <Button color="primary" variant="contained" onClick={fuckArrowFunctions => theCall()}>Get it.</Button>
          <Button color="primary" variant="contained" onClick={fuckArrowFunctions => postTest()}>Post it.</Button>
          <p id="theResult" />
          <p id="thePostResult" />
          <Grid container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={3}>
            <Grid item xs={2}>
              <Paper className={classes.paper}>xs=3</Paper>
            </Grid>
            <Grid item xs={8}>
              <Paper className={classes.paper}>xs=9</Paper>
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  );
}
function theCall() {
  axios
    .get(
      "https://ngprislj52.execute-api.us-east-1.amazonaws.com/dev/echo/hello"
    )
    /*.then(res => console.log(res.data))*/
    .then(res => (document.getElementById("theResult").innerHTML = res.data))
    .catch(err => console.log(err));
}
function postTest() {
  axios
    .post(
      "https://ngprislj52.execute-api.us-east-1.amazonaws.com/dev/echo/post",
      {
        inputOne: 123,
        inputTwo: 456
      }
    )
    .then(
      res => (document.getElementById("thePostResult").innerHTML = res.data)
    )
    .catch(err => console.log(err));
}

export default App;